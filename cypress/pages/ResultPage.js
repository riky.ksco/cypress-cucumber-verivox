export class ResultPage {
  totalvalueitemlistedAssertion = () => {
    cy.get("filtered-products-hint").should("be.visible");
  };

  defaultValuesDisplayedAssertion = (defaultValue) => {
    cy.get('[class="product-list comparison-footer-is-open"] product')
      .its("length")
      .then((length) => {
        expect(length).to.equal(defaultValue);
      });
  };

  moreoptionsButtonClick = () => {
    cy.contains("span", " weitere Tarife laden", { timeout: 20000 }).click();
    cy.wait(3000);
  };

  nextDisplayedAssertion = () => {
    cy.get('[class="product-list comparison-footer-is-open"] product')
      .its("length")
      .then((length) => {
        expect(length).to.equal(40);
      });
  };
  scrollAndClick = (lengthComparison, scrollable) => {
    cy.get('[class="product-list comparison-footer-is-open"] product')
      .its("length")
      .then((length) => {
        if (length != lengthComparison) {
          cy.scrollTo("bottom");
          cy.contains("span", " weitere Tarife laden", {
            timeout: 20000,
          }).then(($button) => {
            if ($button.is(":visible")) {
              $button.click();
              cy.wait(2000);
              this.scrollAndClick(lengthComparison);
            }
          });
        } else {
          expect(length).to.equal(Number(lengthComparison));
          cy.contains("span", " weitere Tarife laden").should("not.exist");
        }
      });
  };
}

export const resultPage = new ResultPage();

export class DetailPage {
  priceItems = () => cy.get('[class="group-price"]');
  tariffDetails = () => cy.get('button[class="button-toggle"]');

  verifyTariffItemIsVisible = () => {
    this.priceItems().eq(0).should("exist");
  };

  openViewDetailsItem = () => {
    this.tariffDetails().eq(0).click({ force: true });
  };
  verifySectionIsVisible = (sectionName, AssertionNamesArray) => {
    cy.wait(3000);
    cy.get('ul[class="navigation"]')
      .eq(0)
      .within(() => {
        cy.get(`li:contains("${sectionName}")`).click({ force: true });
      });
    cy.get('div[class="tab-content"]')
      .eq(0)
      .within(() => {
        //cy.get('[class="group"]').eq(0).should("contain", `${AssertionName}`);
        for (let i = 0; i < AssertionNamesArray.length; i++) {
          cy.get('[class="group"]')
            .eq(i)
            .should("contain", `${AssertionNamesArray[i]}`);
        }
      });
  };

  verifyDocSectionIsVisibleAndAssertion = (sectionName, AssertionName) => {
    cy.get('ul[class="navigation"]')
      .eq(0)
      .within(() => {
        cy.get(`li:contains("${sectionName}")`).click({ force: true });
      });
    cy.get('[class="tab-container"]').should("contain", `${AssertionName}`);
  };
  verifyZumOnlineAntragButtonIsVisible = () => {
    cy.get('[class="tab-container"]').within(() => {
      cy.get('button:contains("Zum Online-Antrag")').should("be.visible");
    });
  };
  verifySummaryZumOnlineAntragButtonisVisible = () => {
    cy.get('[class="product-group product-group-action"]').within(() => {
      cy.get('button:contains("Zum Online-Antrag")').should("be.visible");
    });
  };
}

export const detailPage = new DetailPage();

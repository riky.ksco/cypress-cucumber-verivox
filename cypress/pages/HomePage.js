export class HomePage {
  cookieBannerSelector = () => cy.get("#uc-btn-accept-banner");
  pageNavitationItemsSelector = () => cy.get('[class="page-navigation-items"]');
  gridRowSelector = () => cy.get('[class="grid-row"]');
  situationGroupSelector = () => cy.get('[name="situationGroup"]');
  ageSelector = () => cy.get('[name="age"]');
  calendarSelector = () => cy.get('input[class="su-calendar-input input"]');
  zipCodeSelector = () => cy.get('input[id="prestep_postcode"]');
  resultPageComparisonSelector = () =>
    cy.get('[class="product-list comparison-footer-is-open"]', {
      timeout: 30000,
    });
  resultFilterSelector = () => {
    cy.get('[class="filter"]');
  };

  productItems = () =>
    cy.get('[class="product-list comparison-footer-is-open"] product');

  clearStorage = () => {
    cy.window().then((win) => {
      win.sessionStorage.clear();
    });
  };
  clearCache = () => {
    cy.clearCookies();
  };

  visitURL = (url) => {
    cy.visit(`${url}`);
  };

  collectIntercept = () => {
    return cy
      .intercept("GET", "**https://gts.verivox.de/g/collect**")
      .as("CollectIntercept");
  };

  preSettingsFormIntercept = () => {
    return cy
      .intercept(
        "GET",
        "https://service.verivox.de/applications/insurance/service/config/form/**/pli/preSettings"
      )
      .as("PreSettingsFormIntercept");
  };
  waitForReturnResponse = (nameOfAliasFromIntercept) => {
    return cy
      .wait(`@${nameOfAliasFromIntercept}`)
      .its("response.statusCode")
      .should("eq", 200);
  };

  participatingIntercept = () => {
    return cy
      .intercept(
        "GET",
        "https://service.verivox.de/applications/insurance/service/company/pli/company/participating?provider=**"
      )
      .as("ParticipatingIntercept");
  };

  CookieConfirmationModal = () => {
    cy.wait(3000);
    this.cookieBannerSelector().click({ force: true });
  };

  selectPrivathaftpflicht = () => {
    this.pageNavitationItemsSelector().within(() => {
      cy.get("li")
        .eq(5)
        .within(() => {
          this.gridRowSelector()
            .invoke("css", "display", "block")
            .find('[class="page-navigation-col"]')
            .eq(0)
            .invoke("css", "visibility", "visible")
            .within(() => {
              cy.contains("Privathaftpflicht").click({ force: true });
            });
        });
    });
  };

  selectSituationGroup = (situationStatus) => {
    this.situationGroupSelector().select(`${situationStatus}`);
  };

  typeAge = (age) => {
    this.ageSelector().type(`${age}`);
  };
  jetztvergleichenClick = () => {
    cy.findAllByText("Jetzt vergleichen").click();
  };

  privathaftpflichtAssertion = () => {
    cy.url().should("contains", "vergleich/#/pli/customer?");
    cy.findAllByText("Privathaftpflicht").should("be.visible");
  };

  birthdayPickUp = (BD) => {
    cy.findAllByText("Ihr Geburtsdatum").should("be.visible");
    this.calendarSelector().type(`${BD}`);
  };
  zipcodeSelection = (zipCode) => {
    this.zipCodeSelector().type(`${zipCode}`);
  };

  jetztVergleichenClick = () => {
    cy.wait(3000);
    cy.findByText("Jetzt vergleichen", { timeout: 10000 })
      .should(($el) => {
        expect(Cypress.dom.isDetached($el)).to.eq(false);
      })
      .click({ force: true });
  };
  resultPageAssertions = () => {
    cy.url().should("contains", "/vergleich/#/pli/results");
    cy.get('[class="filter"]').should("be.visible");
    cy.get('[class="product-list comparison-footer-is-open"]', {
      timeout: 20000,
    }).should("be.visible");
  };

  leastitemsAmountAssertion = (amount) => {
    this.productItems().should("have.length.at.least", amount);
  };

  scrollAndClick = (lengthComparison) => {
    this.productItems()
      .its("length")
      .then((length) => {
        if (length != lengthComparison) {
          cy.scrollTo("bottom");
          cy.contains("span", " weitere Tarife laden", { timeout: 20000 }).then(
            ($button) => {
              if ($button.is(":visible")) {
                $button.click();
                cy.wait(2000);
                this.scrollAndClick(lengthComparison);
              }
            }
          );
        } else {
          expect(length).to.equal(Number(lengthComparison));
          cy.contains("span", " weitere Tarife laden").should("not.exist");
        }
      });
  };
}

export const homePage = new HomePage();

Feature: Verivox Insurance Comparison
  As a user
  I want to verify the details of tariffs on Verivox
  So that I can choose the best option for me

  Scenario: Verify offer details for a selected tariff
    Given the same tariff calculation criteria from scenario 1
    And I display the tariff result list page
    Then I should see the tariff price of the first tariff
    When I open tariff details
    Then I see tariff details sections: “Weitere Leistungen”, “Allgemein“, „ Tätigkeiten und Hobbys“
    And I see tariff details sections: “Miete & Immobilien” and “Dokumente”
    And I see the ZUM ONLINE-ANTRAG button

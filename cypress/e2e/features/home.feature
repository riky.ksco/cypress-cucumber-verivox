Feature: Verivox Insurance Comparison
  As a user
  I want to compare insurance tariffs on Verivox
  So that I can choose the best option for me

  Scenario: Compare Privathaftpflicht tariffs
    Given I can open www.verivox.de
    When I navigate to Versicherungen and select Privathaftpflicht
    And I enter my age and Single ohne Kinder
    Then I go to the Privathaftpflicht personal information page
    And I enter my birthdate
    And I enter my zip code
    And I click the Jetzt vergleichen button
    Then I should see a page that lists the available tariffs for my selection
    And at least 5 tariffs are shown

Feature: Verivox Insurance Comparison
  As a user
  I want to compare insurance tariffs on Verivox
  So that I can choose the best option for me

  Scenario: Load multiple tariff result pages
    Given the same tariff calculation criteria from scenario 1
    When I display the tariff Result List page
    Then I should see the total number of available tariffs listed above all the result list
    When I scroll to the end of the result list page
    Then I should see only the first 20 tariffs displayed
    When I click on the button labeled 20 weitere Tarife laden
    Then I should see the next 20 tariffs displayed
    And I can continue to load any additional tariffs until all tariffs have been displayed

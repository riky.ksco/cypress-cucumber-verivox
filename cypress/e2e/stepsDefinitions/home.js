import { Given, When, Then, Before } from "cypress-cucumber-preprocessor/steps";
import { homePage } from "../../pages/HomePage";

Before(() => {
  homePage.collectIntercept();
  homePage.preSettingsFormIntercept();
  homePage.participatingIntercept();
});
Given("I can open www.verivox.de", () => {
  homePage.visitURL("https://www.verivox.de/");
});

When("I navigate to Versicherungen and select Privathaftpflicht", () => {
  homePage.waitForReturnResponse("CollectIntercept");
  homePage.CookieConfirmationModal();
  homePage.selectPrivathaftpflicht();
});

When("I enter my age and Single ohne Kinder", () => {
  homePage.selectSituationGroup("Familie mit Kindern");
  homePage.typeAge("34");
});

Then("I go to the Privathaftpflicht personal information page", () => {
  homePage.jetztvergleichenClick();
  homePage.waitForReturnResponse("PreSettingsFormIntercept");
  homePage.privathaftpflichtAssertion();
});

Then("I enter my birthdate", () => {
  homePage.birthdayPickUp("10.01.1989");
});

Then("I enter my zip code", () => {
  homePage.zipcodeSelection("10715");
});

Then("I click the Jetzt vergleichen button", () => {
  homePage.jetztVergleichenClick();
});

Then(
  "I should see a page that lists the available tariffs for my selection",
  () => {
    homePage.waitForReturnResponse("ParticipatingIntercept");
    homePage.resultPageAssertions();
  }
);

Then("at least 5 tariffs are shown", () => {
  homePage.leastitemsAmountAssertion(5);
});

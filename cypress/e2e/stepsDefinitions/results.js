import { Given, When, Then, Before } from "cypress-cucumber-preprocessor/steps";
import { homePage } from "../../pages/HomePage";
import { resultPage } from "../../pages/ResultPage";

Before(() => {
  homePage.collectIntercept();
  homePage.preSettingsFormIntercept();
  homePage.participatingIntercept();
});
Given("the same tariff calculation criteria from scenario 1", () => {
  homePage.visitURL("https://www.verivox.de/");
  homePage.waitForReturnResponse("CollectIntercept");
  homePage.CookieConfirmationModal();
  homePage.selectPrivathaftpflicht();
  homePage.selectSituationGroup("Familie mit Kindern");
  homePage.typeAge("34");
  homePage.jetztvergleichenClick();
  homePage.waitForReturnResponse("PreSettingsFormIntercept");
  homePage.privathaftpflichtAssertion();
  homePage.birthdayPickUp("10.01.1989");
  homePage.zipcodeSelection("10715");
  homePage.jetztVergleichenClick();
  homePage.waitForReturnResponse("ParticipatingIntercept");
  homePage.resultPageAssertions();
});
When("I display the tariff Result List page", () => {
  homePage.leastitemsAmountAssertion(5);
});

Then(
  "I should see the total number of available tariffs listed above all the result list",
  () => {
    resultPage.totalvalueitemlistedAssertion();
  }
);

When("I scroll to the end of the result list page", () => {
  cy.scrollTo("bottom");
});

Then("I should see only the first 20 tariffs displayed", () => {
  resultPage.defaultValuesDisplayedAssertion(20);
});

When("I click on the button labeled 20 weitere Tarife laden", () => {
  resultPage.moreoptionsButtonClick();
});

Then("I should see the next 20 tariffs displayed", () => {
  resultPage.nextDisplayedAssertion();
});

When(
  "I can continue to load any additional tariffs until all tariffs have been displayed",
  () => {
    cy.get("filtered-products-hint")
      .find("> span")
      .invoke("text")
      .then((text) => {
        const firstWord = text.split(" ")[1];
        homePage.scrollAndClick(firstWord);
      });
  }
);

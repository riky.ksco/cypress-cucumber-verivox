import { Given, When, Then, Before } from "cypress-cucumber-preprocessor/steps";
import { homePage } from "../../pages/HomePage";
import { detailPage } from "../../pages/DetailPage";
import {
  Allgemein,
  UnHobbys,
  MietteImobilen,
} from "../../fixtures/example.json";

Before(() => {
  homePage.clearCache();
  homePage.clearStorage();
  homePage.collectIntercept();
  homePage.preSettingsFormIntercept();
  homePage.participatingIntercept();
});
Given("the same tariff calculation criteria from scenario 1", () => {
  homePage.visitURL("https://www.verivox.de/");
  homePage.waitForReturnResponse("CollectIntercept");
  homePage.CookieConfirmationModal();
  homePage.selectPrivathaftpflicht();
  homePage.selectSituationGroup("Familie mit Kindern");
  homePage.typeAge("34");
  homePage.jetztvergleichenClick();
  homePage.waitForReturnResponse("PreSettingsFormIntercept");
  homePage.privathaftpflichtAssertion();
  homePage.birthdayPickUp("10.01.1989");
  homePage.zipcodeSelection("10715");
  homePage.jetztVergleichenClick();
  homePage.waitForReturnResponse("ParticipatingIntercept");
  homePage.resultPageAssertions();
});
When("I display the tariff result list page", () => {
  homePage.leastitemsAmountAssertion(5);
});

Then("I should see the tariff price of the first tariff", () => {
  detailPage.verifyTariffItemIsVisible();
});

When("I open tariff details", () => {
  detailPage.openViewDetailsItem();
});

Then(
  "I see tariff details sections: “Weitere Leistungen”, “Allgemein“, „ Tätigkeiten und Hobbys“",
  () => {
    detailPage.verifySectionIsVisible("All­ge­mein", Allgemein);
    detailPage.verifyZumOnlineAntragButtonIsVisible();
    detailPage.verifySectionIsVisible("Tä­tig­kei­ten und Hob­bys", UnHobbys);
    detailPage.verifyZumOnlineAntragButtonIsVisible();
  }
);

When(
  "I see tariff details sections: “Miete & Immobilien” and “Dokumente”",
  () => {
    detailPage.verifySectionIsVisible("Miete & Im­mo­bi­li­en", MietteImobilen);
    detailPage.verifyZumOnlineAntragButtonIsVisible();

    detailPage.verifyDocSectionIsVisibleAndAssertion(
      "Do­kumen­te",
      "Be­leh­rung und Wi­der­rufs­recht.pdf"
    );
    detailPage.verifyZumOnlineAntragButtonIsVisible();
  }
);

When("I see the ZUM ONLINE-ANTRAG button", () => {
  detailPage.verifySummaryZumOnlineAntragButtonisVisible();
});
